# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  # Set up libvirt provider.
  config.vm.provider "libvirt" do |lv|
    lv.cpus = 4
    lv.memory = 4000
  end

  config.vm.define "gitlab" do |gl|
    gl.vm.box = "archlinux/archlinux"

    # Disable the default synced folder.
    # https://developer.hashicorp.com/vagrant/docs/synced-folders/basic_usage#disabling
    gl.vm.synced_folder ".", "/vagrant", disabled: true

    # Deregister the runner when the VM is destroyed.
    config.trigger.before [:destroy] do |tg|
      tg.info = "===== Deregistering this runner"
      tg.run_remote = {
        inline: <<-SHELL
        runnertoken=$(grep -Po "(?<=token = \\").+(?=\\"$)" /etc/gitlab-runner/config.toml)
        gitlab-runner unregister \
          --config /etc/gitlab-runner/config.toml \
          --url "https://gitlab.com/" \
          --token "${runnertoken}"
      SHELL
    }
    end

    # OS provisioning
    config.vm.provision "shell" do |s|
      s.env = { token: ENV['runnertoken'] }
      s.sensitive = true
      s.inline = <<-SHELL
        hostnamectl set-hostname gitlab-runner
        echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
        locale-gen
        echo "LANG=en_US.UTF-8" > /etc/locale.conf
        pacman -Syu --noconfirm --noprogressbar \
          fuse-overlayfs \
          gitlab-runner \
          htop \
          podman \
          qemu-user-static \
          qemu-user-static-binfmt \
          slirp4netns
        echo "loop" >> /etc/modules-load.d/loop.conf
        echo "FF_NETWORK_PER_BUILD=1" >> /etc/environment
        systemctl enable --now podman.socket gitlab-runner
        echo "===== registering runner"
        gitlab-runner register \
          --non-interactive \
          --description "archlinux-vagrant" \
          --url "https://gitlab.com/" \
          --executor "docker" \
          --docker-host "unix:///run/podman/podman.sock" \
          --docker-image quay.io/podman/stable:latest \
          --docker-privileged="true" \
          --docker-pull-policy="always" \
          --tag-list "selfhosted,vagrant,archlinux" \
          --registration-token "${token}"
        sed -i 's,concurrent = .*,concurrent = 10,' /etc/gitlab-runner/config.toml
        reboot
      SHELL
    end
  end
end
