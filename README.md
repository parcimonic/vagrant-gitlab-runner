# GitLab runner with Vagrant+libvirt

This is a simple vagrantfile that can be used to start a GitLab runner for your projects. If you have a powerful computer at home, you can use it to avoid consuming GitLab free tier minutes for large build pipelines.

## Required plugin

This VM uses the libvirt plugin for Vagrant, you can read more about it in the [project's repository](https://github.com/vagrant-libvirt/vagrant-libvirt) and in the [Arch wiki](https://wiki.archlinux.org/title/Vagrant#vagrant-libvirt) page.

## Required configuration

The runner registration token must be set by exporting the env var `runnertoken=<your_token_here>` in the *host* shell. You may also want to change CPU/RAM allocation for the VM or the list of tags that will be associated with this runner (`--tag-list` value).

Read more about GitLab runner configuration in the [docs](https://docs.gitlab.com/runner/).

## Starting the runner

1. `export runnertoken=<your_token_here>`
2. `vagrant up`

The runner should appear in your "runners" section of GitLab after it has finished booting.

## Podman as the job executor

I've replaced Docker with Podman for the job executor, you can read more about the GitLab setup
[here](https://docs.gitlab.com/runner/executors/docker.html#use-podman-to-run-docker-commands) and
learn more about Podman in the [official docs](https://docs.podman.io) (also [Arch Wiki](https://wiki.archlinux.org/title/Podman)).

The default executor image is `quay.io/podman/stable:latest`, which uses [DNF](https://dnf.readthedocs.io) as the package manager. Be mindful of that if you don't override the image in your `.gitlab-ci.yml` file.
